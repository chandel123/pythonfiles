a= {}
print(type(a))


student= {
    'name':'aneeta',
    'roll_no':'12',
    'class':'python',
    'subject':['php','python','c++'],
    'tech':{'front':['html','css','js'],'backend':['python','sql','django']},
}

print(student)
print(student['name'])
print(student['roll_no'])
print(student['class'])
print(student['subject'])
print(student['subject'][2])
print(student['tech']['backend'][0])