a=(1,2,3,'python')
b= 100,200
#### INDEXING AND SLICING.......
print(a[1:])
print(a[-1:])
print(a[:2])
print(a[:-2])
print(a[::2])

############# LENGTH TUPPLE.......
print(len(a))


#########   CONCATENATE.........
c=a+b
print(c)


############    ITERATION.......
d=b*3
print(d)

#############   Membership.......
print(201 in a)
print(201 not in a)


##########  Search......
s = a.index(3)
print('index of python  in tupple a is {}'.format(s))


############   Count Element............
total= a.count(3)
print(total)

#print(type(a))
